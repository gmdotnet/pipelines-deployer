<?php
namespace Deployer;
require 'recipe/common.php';
require 'vendor/gmdotnet/deployer-recipes/recipes/git.php';

// Configuration
set('repository', 'git@bitbucket.org:your-user/your-repository.git');

// Servers
$dir = dirname(__FILE__);
serverList($dir.'/servers.yml');

// Tasks
task('project:afterpull', function(){
    cd('{{deploy_path}}/htdocs');
    run('chown www-data:www-data {{deploy_path}}/htdocs -R');
});

// Tasks
desc('Deploy your-project');
task('proejct:deploy', [
    'git:pull',
    'project:afterpull'
]);

after('proejct:deploy', 'success');